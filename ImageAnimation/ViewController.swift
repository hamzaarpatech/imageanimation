//
//  ViewController.swift
//  ImageAnimation
//
//  Created by Hamza on 3/13/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var leftAnchor : NSLayoutConstraint?
    var rightAnchor : NSLayoutConstraint?
    var topAnchor : NSLayoutConstraint?
    var bottomAnchor : NSLayoutConstraint?
    var widthAnchor : NSLayoutConstraint?
    var heightAnchor : NSLayoutConstraint?
    
   lazy var gokuImage : UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "goku"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleMethod)))
        return imageView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(gokuImage)
    
        topAnchor = gokuImage.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor)
        topAnchor?.isActive = true
        leftAnchor = gokuImage.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor)
        leftAnchor?.isActive = true
        
        rightAnchor = gokuImage.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor)
        bottomAnchor = gokuImage.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)
        
        widthAnchor = gokuImage.widthAnchor.constraint(equalToConstant: 150)
        widthAnchor?.isActive = true
        
        heightAnchor = gokuImage.heightAnchor.constraint(equalToConstant: 150)
        heightAnchor?.isActive = true
    }
    
    @objc func handleMethod(){
        
        topAnchor?.isActive = false
        leftAnchor?.isActive = false
        rightAnchor?.isActive = true
        bottomAnchor?.isActive = true
        
        widthAnchor?.constant = 200
        heightAnchor?.constant = 200
        
        rightAnchor?.constant = -16
        bottomAnchor?.constant = -16
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
}

